﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace huddleCodeA2
{
    public partial class Form1 : Form
    {
        Routes routes_;
       
        public Form1()
        {
            InitializeComponent();

            routes_ = new Routes();
            routes_.addRoute(new Route('A', 'B', 5));
            routes_.addRoute(new Route('B', 'C', 4));
            routes_.addRoute(new Route('C', 'D', 7));
            routes_.addRoute(new Route('D', 'C', 8));
            routes_.addRoute(new Route('D', 'E', 6));
            routes_.addRoute(new Route('A', 'D', 5));
            routes_.addRoute(new Route('C', 'E', 2));
            routes_.addRoute(new Route('E', 'B', 3));
            routes_.addRoute(new Route('A', 'E', 7));
        }

        private void getDistance_btn_Click(object sender, EventArgs e)
        {
            Routes routes = new Routes();
            String[] stops = stops_15_txt.Text.Split('-');
            length_15_txt.Text = "" + 0;
            bool flag = false;

            try
            {
                for (int i = 0; i < stops.Count() - 1; i++)
                {
                    // Grabs the first 2 values
                    char sStop = Char.Parse(stops.ElementAt(i));
                    char eStop = Char.Parse(stops.ElementAt(i + 1));
                    IEnumerable<Route> check = routes_.getRoutes().Where(data => data.getStart().Equals(sStop) && data.getEnd().Equals(eStop));

                    if (check.Count() != 0)
                    {
                        routes.addRoute(check.First());
                    }
                    else
                    {
                        MessageBox.Show("NO SUCH ROUTE");
                        flag = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                flag = true;
            }

            if (!flag)
                length_15_txt.Text = "" + routes.outputRouteLength();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<Routes> routeCount = new List<Routes>();
            routeCount = questionSix('C', 'C', 'C', 3, routeCount, new Routes());
            six_lbl.Text = "" + routeCount.Distinct().Count();

            int shortest = routeCount.First().outputRouteLength();


            List<Routes> routeCountEight = new List<Routes>();
            routeCountEight = questionSix('A', 'C', 'A', 3, routeCountEight, new Routes());
            foreach (Routes routes in routeCountEight.Distinct())
            {
                if (shortest > routes.outputRouteLength())
                    shortest = routes.outputRouteLength();
            }
            eight_lbl.Text = "" + shortest;
        }

        private List<Routes> questionSix(char newStart, char end, char start, int noOfJunctions, List<Routes> routeCount, Routes rCount)
        {

            IEnumerable<Route> startRoutes = routes_.getRoutes().Where(data => data.getStart().Equals(newStart));
            foreach (Route rou in startRoutes)
            {
                rCount.addRoute(rou);
                if (rou.getEnd().Equals(end) )//&& rCount.getRoutes().First().getStart().Equals(start))
                { // Add route if it matches conditions
                    if (rCount.outputNumberOfJunctions() <= noOfJunctions)
                    {
                        List<Routes> temp = new List<Routes>();
                        temp.Add(rCount);
                        return temp;
                    }
                }
                else
                {
                        routeCount.AddRange(questionSix(rou.getEnd(), end, start, noOfJunctions, routeCount, rCount));
                }

                rCount = new Routes();
            }

            return routeCount;
        }
    }
}
