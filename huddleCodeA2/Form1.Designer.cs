﻿namespace huddleCodeA2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stops_15_txt = new System.Windows.Forms.TextBox();
            this.oneToSixTitle_lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.getDistance_15_btn = new System.Windows.Forms.Button();
            this.length_15_txt = new System.Windows.Forms.Label();
            this.six_lbl = new System.Windows.Forms.Label();
            this.six_numberofroutes_lbl = new System.Windows.Forms.Label();
            this.six_Title_lbl = new System.Windows.Forms.Label();
            this.eight_lbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // stops_15_txt
            // 
            this.stops_15_txt.Location = new System.Drawing.Point(18, 95);
            this.stops_15_txt.Name = "stops_15_txt";
            this.stops_15_txt.Size = new System.Drawing.Size(446, 20);
            this.stops_15_txt.TabIndex = 0;
            // 
            // oneToSixTitle_lbl
            // 
            this.oneToSixTitle_lbl.AutoSize = true;
            this.oneToSixTitle_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneToSixTitle_lbl.Location = new System.Drawing.Point(12, 28);
            this.oneToSixTitle_lbl.Name = "oneToSixTitle_lbl";
            this.oneToSixTitle_lbl.Size = new System.Drawing.Size(72, 31);
            this.oneToSixTitle_lbl.TabIndex = 1;
            this.oneToSixTitle_lbl.Text = "1 - 5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(306, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "DISTANCE BETWEEN STOPS: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // getDistance_15_btn
            // 
            this.getDistance_15_btn.Location = new System.Drawing.Point(18, 121);
            this.getDistance_15_btn.Name = "getDistance_15_btn";
            this.getDistance_15_btn.Size = new System.Drawing.Size(446, 23);
            this.getDistance_15_btn.TabIndex = 4;
            this.getDistance_15_btn.Text = "Get Distance";
            this.getDistance_15_btn.UseVisualStyleBackColor = true;
            this.getDistance_15_btn.Click += new System.EventHandler(this.getDistance_btn_Click);
            // 
            // length_15_txt
            // 
            this.length_15_txt.AutoSize = true;
            this.length_15_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.length_15_txt.Location = new System.Drawing.Point(406, 68);
            this.length_15_txt.Name = "length_15_txt";
            this.length_15_txt.Size = new System.Drawing.Size(21, 24);
            this.length_15_txt.TabIndex = 5;
            this.length_15_txt.Text = "0";
            this.length_15_txt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // six_lbl
            // 
            this.six_lbl.AutoSize = true;
            this.six_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.six_lbl.Location = new System.Drawing.Point(406, 191);
            this.six_lbl.Name = "six_lbl";
            this.six_lbl.Size = new System.Drawing.Size(21, 24);
            this.six_lbl.TabIndex = 8;
            this.six_lbl.Text = "0";
            this.six_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // six_numberofroutes_lbl
            // 
            this.six_numberofroutes_lbl.AutoSize = true;
            this.six_numberofroutes_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.six_numberofroutes_lbl.Location = new System.Drawing.Point(14, 191);
            this.six_numberofroutes_lbl.Name = "six_numberofroutes_lbl";
            this.six_numberofroutes_lbl.Size = new System.Drawing.Size(314, 32);
            this.six_numberofroutes_lbl.TabIndex = 7;
            this.six_numberofroutes_lbl.Text = "NUMBER OF ROUTES THAT START\r\nWITH C AND END WITH C W/ MAX 3 JUNCS";
            this.six_numberofroutes_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // six_Title_lbl
            // 
            this.six_Title_lbl.AutoSize = true;
            this.six_Title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.six_Title_lbl.Location = new System.Drawing.Point(12, 151);
            this.six_Title_lbl.Name = "six_Title_lbl";
            this.six_Title_lbl.Size = new System.Drawing.Size(38, 31);
            this.six_Title_lbl.TabIndex = 6;
            this.six_Title_lbl.Text = "6 ";
            // 
            // eight_lbl
            // 
            this.eight_lbl.AutoSize = true;
            this.eight_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eight_lbl.Location = new System.Drawing.Point(407, 261);
            this.eight_lbl.Name = "eight_lbl";
            this.eight_lbl.Size = new System.Drawing.Size(21, 24);
            this.eight_lbl.TabIndex = 11;
            this.eight_lbl.Text = "0";
            this.eight_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(41, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(272, 32);
            this.label3.TabIndex = 10;
            this.label3.Text = "LENGTH OF THE SHORTEST ROUTE\r\nFROM A TO C\r\n";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 31);
            this.label4.TabIndex = 9;
            this.label4.Text = "8";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 470);
            this.Controls.Add(this.eight_lbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.six_lbl);
            this.Controls.Add(this.six_numberofroutes_lbl);
            this.Controls.Add(this.six_Title_lbl);
            this.Controls.Add(this.length_15_txt);
            this.Controls.Add(this.getDistance_15_btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.oneToSixTitle_lbl);
            this.Controls.Add(this.stops_15_txt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox stops_15_txt;
        private System.Windows.Forms.Label oneToSixTitle_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button getDistance_15_btn;
        private System.Windows.Forms.Label length_15_txt;
        private System.Windows.Forms.Label six_lbl;
        private System.Windows.Forms.Label six_numberofroutes_lbl;
        private System.Windows.Forms.Label six_Title_lbl;
        private System.Windows.Forms.Label eight_lbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

