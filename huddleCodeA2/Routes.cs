﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace huddleCodeA2
{
    class Routes 
    {
        List<Route> routes_;

        public Routes()
        {
            routes_ = new List<Route>();
        }

        public List<Route> getRoutes()
        {
            return routes_;
        }

        public void addRoute(Route route)
        {
            routes_.Add(route);
        }

        public int outputRouteLength()
        {
            int length = 0; 

            foreach(Route rou in routes_)
            {
                length += rou.getLength();
            }

            return length;
        }

        public String outputRouteStops()
        {
            String outputted = "";
            outputted = ""+ routes_.ElementAt(0).getStart();

            for(int i = 1; i < routes_.Count(); i++)
            {
                outputted = outputted + " - " + routes_.ElementAt(i).getEnd();
            }

            return outputted;
        }

        public int outputNumberOfJunctions()
        {
            //    List<char> allJunctions = new List<char>();

            //    foreach(Route route in routes_)
            //    {
            //        allJunctions.Add(route.getStart());
            //        allJunctions.Add(route.getEnd());
            //    }

            //    return allJunctions.Distinct().Count();

            return routes_.Count();

        }

    }
}
