﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace huddleCodeA2
{
    class Route
    {
        char start_, end_;
        int length_;

        public Route(char start, char end, int length)
        {
            start_  = start;
            end_    = end;
            length_ = length;
        }

        public char getStart()  { return start_; }
        public char getEnd()    { return end_; }
        public int getLength()  { return length_; }
    }
}
